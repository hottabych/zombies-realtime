﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverObserver : MonoBehaviour
{
    public PlayerHealth playerHealth;       // Reference to the player's health.
    public float restartDelay = 5f;         // Time to wait before restarting the level        
    float restartTimer;                     // Timer to count up to restarting the level        

    void Update()
    {
        // If the player has run out of health...
        if (playerHealth.currentHealth <= 0 || LevelManager.instance.emblemStolen == true)
        {
            // .. increment a timer to count up to restarting.
            restartTimer += Time.deltaTime;

            // .. if it reaches the restart delay...
            if (restartTimer >= restartDelay)
            {
                // .. then reload the currently loaded level.
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}
