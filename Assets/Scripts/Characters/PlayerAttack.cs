﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public float attackRange;
    public float timeBetweenAttacks;
    public int attackDamage = 1;    

    float timer;
    Transform enemyInRange;

    Animator anim;
    new AudioSource audio;
    private PlayerAudio audioManager;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        audioManager = GetComponent<PlayerAudio>();
    }


    private void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetButtonDown("Fire1") && timer >= timeBetweenAttacks)
        {           
            timer = 0f;
            Animate();
        }
    }

    public void Attack()       //из ивента анимации
    {
        audio.PlayOneShot(audioManager.swordClips[Random.Range(0, 2)]);

        if (enemyInRange)
        {
            ZombieHealth enemyHealth = enemyInRange.GetComponent<ZombieHealth>();
            if (enemyHealth)
            {
                enemyHealth.TakeDamage(attackDamage);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            enemyInRange = other.transform;            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            enemyInRange = null;           
        }
    }

    void Animate()
    {
        anim.SetTrigger("Attack");
    }
}
