﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;            // The amount of health the enemy starts the game with.
    public int currentHealth;                   // The current health the enemy has.

    Animator anim;
    new AudioSource audio;
    private PlayerAudio audioManager;

    bool isDead;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        audioManager = GetComponent<PlayerAudio>();

        currentHealth = startingHealth;
    }


    public void TakeDamage(int amount)
    {
        if (isDead)
            return;        

        currentHealth -= amount;
        UiManager.instance.healthBar.value -= amount / 100f;

        if (currentHealth <= 0)
        {
            Death();
        }
        else
        {
            anim.SetTrigger("Hit");
            audio.PlayOneShot(audioManager.ouchClip);
        }
    }


    void Death()
    {        
        isDead = true;        
        anim.SetTrigger("Dead");
        audio.PlayOneShot(audioManager.dieClip);
        GetComponent<PlayerMove>().enabled = false;
    }
}
