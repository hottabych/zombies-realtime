﻿using UnityEngine;
using UnityEngine.AI;

public class ZombieHealth : MonoBehaviour
{
    public int startingHealth = 3;            // The amount of health the enemy starts the game with.
    public int currentHealth;                   // The current health the enemy has.
    public float destroyTime = 2f;
    public bool isDead;

    Animator anim;
    new AudioSource audio;
    NavMeshAgent agent;
    Transform goal;
    private ZombieAudio audioManager;


    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        goal = GameObject.FindGameObjectWithTag("Goal").transform;
        audioManager = GetComponent<ZombieAudio>();
        currentHealth = startingHealth;
    }

    public void TakeDamage(int amount)
    {
        if (isDead) return;

        currentHealth -= amount;

        if (currentHealth <= 0)
        {
            Death();
        }
        else
        {
            audio.PlayOneShot(audioManager.ouchClips[Random.Range(0,2)]);
            anim.SetTrigger("Hit");
        }
    }

    void Death()
    {        
        isDead = true;        
        anim.SetTrigger("Dead");
        audio.PlayOneShot(audioManager.dieClips[Random.Range(0, 2)]);
        agent.enabled = false;
        if (GetComponent<ZombieMovement>().hasGoal)
        {
            goal.parent = null;
            goal.transform.position = goal.GetComponent<Emblem>().startPosition;
        }        
        Destroy(gameObject, destroyTime);
    }

}
