﻿using UnityEngine;

public class ZombieAttack : MonoBehaviour
{
    bool playerInRange;
    float timer;
    public float timeBetweenAttacks;
    public int attackDamage = 10;
    Transform player;
    Animator anim;

    ZombieMovement zombieMove;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        anim = GetComponent<Animator>();
        zombieMove = GetComponent<ZombieMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = true;
            if (zombieMove.zombieState != ZombieState.RunHome)  //если он не убегает с эмблемой
                zombieMove.zombieState = ZombieState.Attack;    //после первого попадания в триггер зомби начинает преследовать игрока
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = false;
        }
    }


    private void Update()
    {
        if (zombieMove.zombieState == ZombieState.Attack)
        {
            timer += Time.deltaTime;

            if (timer >= timeBetweenAttacks)
            {                
                timer = 0f;
                if (!GetComponent<ZombieHealth>().isDead)
                    Animate();
            }
        }
    }

    public void Attack()        //вызов из ивента анимации
    {
        if (playerInRange)
        {
            PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();
            if (playerHealth)
            {
                playerHealth.TakeDamage(attackDamage);
            }
        }
    }


    void Animate()
    {
        anim.SetInteger("AttackRnd", Random.Range(0, 4));
        anim.SetTrigger("Attack");
    }
}
