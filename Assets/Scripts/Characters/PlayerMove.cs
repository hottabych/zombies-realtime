﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;

    public bool invertAxis;
    public float speed = 1f;
    public float turnSmoothing = 15f;
    Vector3 movement;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        if (invertAxis)
        {
            h = -h;
            v = -v;
        }

        if (h != 0f || v != 0f)
        {
            Move(h, v);
            Turning(h, v);
        }

        Animating(h, v);
    }

    void Move(float horizontal, float vertical)
    {
        movement.Set(horizontal, 0f, vertical);

        movement = movement.normalized * speed * Time.deltaTime;

        rb.MovePosition(rb.position + movement);
    }        

    void Turning(float horizontal, float vertical)
    {
        // Create a new vector of the horizontal and vertical inputs.
        Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);

        // Create a rotation based on this new vector assuming that up is the global y axis.
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

        // Create a rotation that is an increment closer to the target rotation from the player's rotation.
        Quaternion newRotation = Quaternion.Lerp(rb.rotation, targetRotation, turnSmoothing * Time.deltaTime);

        // Change the players rotation to this new rotation.
        rb.MoveRotation(newRotation);
    }

    void Animating (float horizontal, float vertical)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = horizontal != 0f || vertical != 0f;

        // Tell the animator whether or not the player is walking.
        anim.SetBool("IsWalking", walking);
    }
}
