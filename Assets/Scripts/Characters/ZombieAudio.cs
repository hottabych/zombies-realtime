﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAudio : MonoBehaviour
{
    public AudioClip[] ouchClips;
    public AudioClip[] dieClips;
}
