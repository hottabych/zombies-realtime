﻿using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class ZombieMovement : MonoBehaviour
{
    public ZombieState zombieState = ZombieState.SeekGoal;

    ZombieHealth health;
    PlayerHealth playerHealth;
    NavMeshAgent agent;
    public float agentRunSpeed = 2.5f;
    Transform player;
    Transform goal;
    Transform home;
    Animator anim;

    public float sqrGoalDistanceThreshold = 2.5f;
    public bool hasGoal;

    public event Action OnGoalReached;    


    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = player.GetComponent<PlayerHealth>();
        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<ZombieHealth>();
        anim = GetComponent<Animator>();
        anim.SetInteger("Walk", Random.Range(0, 2));        
        goal = GameObject.FindGameObjectWithTag("Goal").transform;
        //берем все точки спауна, выбираем одну случайную
        var spawnpoints = GameObject.FindGameObjectsWithTag("Spawn");
        home = spawnpoints[Random.Range(0, spawnpoints.Length)].transform;
    }

    private void Start()
    {
        OnGoalReached += () =>
        {
            hasGoal = true;
            goal.transform.parent = transform;
            goal.transform.position = new Vector3(transform.position.x, transform.position.y + 3f, transform.position.z);
            anim.SetTrigger("Run");
            LevelManager.instance.GetComponent<AudioSource>().PlayOneShot(LevelManager.instance.stolenClip);
            zombieState = ZombieState.RunHome;          
        };

        agent.SetDestination(goal.position);
    }

    void Update ()
    {
        if (zombieState == ZombieState.SeekGoal)
        {
            if (health.currentHealth > 0f)
            {
                float sqrDistToGoal = (transform.position - goal.position).sqrMagnitude;
                if (sqrDistToGoal < sqrGoalDistanceThreshold)
                {
                    OnGoalReached();
                }
            }
        }
        else if (zombieState == ZombieState.Attack)
        {
            if (health.currentHealth > 0f && playerHealth.currentHealth > 0f)
            {                
                agent.SetDestination(player.position);
            }
        }
        else if (zombieState == ZombieState.RunHome)
        {
            if (health.currentHealth > 0f && playerHealth.currentHealth > 0f)
            {
                agent.SetDestination(home.position);
                agent.speed = agentRunSpeed;

                float sqrDistToGoal = (transform.position - home.position).sqrMagnitude;
                if (sqrDistToGoal < sqrGoalDistanceThreshold && hasGoal)
                {
                    if (!LevelManager.instance.emblemStolen)
                        LevelManager.instance.GetComponent<AudioSource>().PlayOneShot(LevelManager.instance.gameoverClip);
                    LevelManager.instance.emblemStolen = true;
                    UiManager.instance.emblemStolen.gameObject.SetActive(true);
                }
            }
        }
    }
}
