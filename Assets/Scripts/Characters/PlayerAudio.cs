﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public AudioClip[] swordClips;
    public AudioClip ouchClip;
    public AudioClip dieClip;
}
