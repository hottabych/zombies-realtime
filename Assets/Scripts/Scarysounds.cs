﻿using UnityEngine;

public class Scarysounds : MonoBehaviour
{
    public AudioClip[] sounds;
    new AudioSource audio;

    public float timerMin, timerMax;
    float _timer;

    private void Awake()
    {
        ResetTimer();

        audio = GetComponent<AudioSource>();
    }

    void ResetTimer()
    {
        _timer = Random.Range(timerMin, timerMax);
    }

    private void Update()
    {
        _timer -= Time.deltaTime;

        if (_timer < 0)
        {
            audio.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
            ResetTimer();
        }
    }

}
