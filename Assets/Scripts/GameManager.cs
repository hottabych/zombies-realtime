﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum GameState
{
    Normal,
    GameOver,
    Menu
}

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
                if (instance == null)
                {
                    instance = new GameManager();
                    DontDestroyOnLoad(instance);
                }
            }
            return instance;
        }
    }

    GameState gameState = GameState.Normal;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
