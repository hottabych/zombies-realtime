﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ссылки на UI
/// </summary>
public class UiManager : MonoBehaviour
{
    public static UiManager instance;

    public Slider healthBar;
    public Text scoreText;
    public Text emblemStolen;

    private void Awake()
    {
        instance = this;
    }
}
