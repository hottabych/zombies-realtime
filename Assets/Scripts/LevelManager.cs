﻿using UnityEngine;

/// <summary>
/// Подсчет очков, времени
/// </summary>
public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    private int score = 0;
    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
            UiManager.instance.scoreText.text = score.ToString();
        }
    }

    public bool emblemStolen;
    public AudioClip stolenClip;
    public AudioClip gameoverClip;

    private void Awake()
    {
        instance = this;        
    }

    private void Start()
    {
        UiManager.instance.scoreText.text = "0";
    }
}
