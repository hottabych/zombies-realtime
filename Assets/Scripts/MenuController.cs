﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public float timer;
    float _timer;
    public GameObject pressStartLabel;

    private void Awake()
    {
        _timer = timer;
    }

    void Update ()
    {
        _timer -= Time.deltaTime;
        if (_timer < 0)
        {
            pressStartLabel.SetActive(!pressStartLabel.activeSelf);
            _timer = timer;
        }

        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene(1);
        }
	}
}
