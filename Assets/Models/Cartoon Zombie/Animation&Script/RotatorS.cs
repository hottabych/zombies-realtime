﻿using UnityEngine;
using System.Collections;

public class RotatorS : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        // ...also rotate around the World's Y axis
        gameObject.transform.Rotate(0, 20 * Time.deltaTime, 0);
    }
}
